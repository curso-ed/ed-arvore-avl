package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore AVL.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreAVL<E extends Comparable<E>> extends ArvoreBinariaPesquisa<E> {
    protected byte fb;

    /**
     * Cria uma árvore AVL com o valor da raiz nulo
     */
    public ArvoreAVL() {
    }

    /**
     * Cria uma árvore AVL cuja raiz armazena o valor especificado.
     * @param valor O valor a ser armazenado
     */
    public ArvoreAVL(E valor) {
        super(valor);
    }

    /**
     * Retorna o fator de balanço deste nó.
     * @return O fator de balanço deste nó
     */
    public byte getFb() {
        return fb;
    }

    /**
     * Inicializa o fator de balaço deste nó.
     * @param fb O fator de balanço a ser atribuído
     */
    protected void setFb(byte fb) {
        this.fb = fb;
    }

    /**
     * Executa a operação de rotação à esquerda em torno do nó especificado.
     * @param x O nó pivo (desregulado)
     * @return O nó que ocupou o lugar de x
     */
    protected ArvoreAVL<E> rotacaoEsquerda(ArvoreAVL<E> x) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Executa a operação de rotação à direita em torno do nó especificado.
     * @param y O nó pivo (desregulado)
     * @return O nó que ocupou o lugar de y
     */
    protected ArvoreAVL<E> rotacaoDireita(ArvoreAVL<E> y) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Executa a rotação para restaurar os fatores de balanço da árvore.
     * @param p O nó desregulado
     * @param q O nó inserido
     * @return A raíz da árvore, possivelmente modificada
     */
    protected ArvoreAVL<E> rotacao(ArvoreAVL<E> p, ArvoreAVL<E> q) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Ajusta o fator de balanço após uma inserção.
     * Retorna o primeiro nó desregulado encontrado.
     * Caso não haja nó desregulado, retorna o último nó cujo fator de
     * balanço foi ajustado.
     * @param no O nó inserido
     * @return O primeiro nó desregulado encontrado ou o primeiro regulado
     */
    protected ArvoreAVL<E> ajustaFbInsercao(ArvoreAVL<E> no) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Ajusta o fator de balanço após uma exclusão.
     * Retorna {@code null} caso não haja nó desregulado.
     * @param no O pai do nó excluído
     * @return O primeiro nó desregulado encontrado ou {@code null}
     */
    protected ArvoreAVL<E> ajustaFbExclusao(ArvoreAVL<E> no) {
        throw new RuntimeException("Não implementado");
    }

    /**
     * Insere um nó na árvore AVL.
     * Compatibiliza o tipo de dado com a super classe.
     * @param no O nó a ser inserido
     * @return O nó inserido
     */
    protected ArvoreAVL<E> insere(ArvoreAVL<E> no) {
        super.insere(no);
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Insere um nó na árvore contendo o valor especificado.
     * @param valor O valor armazenado no nó inserido
     * @return A raiz da árvore, potencialmente modificada
     */
    @Override
    public ArvoreAVL<E> insere(E valor) {
        return insere(new ArvoreAVL<>(valor));
    }
    
    /**
     * Exclui um nó de uma árvore AVL.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore, potencialmente modificada
     */
    public ArvoreAVL<E> exclui(ArvoreAVL<E> no) {
        throw new RuntimeException("Não implementado");
    }
    
}
